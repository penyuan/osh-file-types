<!--
SPDX-FileCopyrightText: 2021 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# OSH related file-type meta-data

[![GitHub license](
    https://img.shields.io/gitlab/license/OSEGermany/osh-file-types.svg?style=flat)](
    LICENSE.txt)
[![REUSE status](
    https://api.reuse.software/badge/gitlab.com/OSEGermany/osh-file-types)](
    https://api.reuse.software/info/gitlab.com/OSEGermany/osh-file-types)

See the data files contianed within this repo.
They shoud lbe self explanatory.

